public class Subject1 {
    public static void main(String[] args) {
        // here we can instantiate objects from the classes below and class functions
        // like A a.metA(), M m.add(B b) - after we create a B object etc.
    }
}

interface C {
}

class A implements C {
    private M m; // composition, the instance of M created inside class A

    public A() {
        this.m = new M(); // create the instance of M
    }

    public void metA() {
        System.out.println("metA");
    }
}

class M {
    private B b; // M contains an instance of B, not created here

    public void addB(B b) {
        this.b = b; // object b not created inside the class M
    }
}

class B {
    public void metB() {
        System.out.println("metB");
    }
}

class L {
    private int a;
    private M m; // L has access to an object of class M

    public void i(X x) {
        // the method uses information from an instance of X, but the class does not contain it
        System.out.println("The info about the instance of X is: " + x.toString());
    }
}

class X {
}
