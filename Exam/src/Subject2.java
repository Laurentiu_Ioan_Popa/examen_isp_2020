import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subject2 extends JFrame {

    private static final String INITIAL_TEXT = "some text";
    private boolean wasSwitched = false;

    JLabel label1; // added labels to differentiate the 2 text fields
    JLabel label2;

    JTextField text1;
    JTextField text2;

    JButton bSwitch;

    public Subject2() {
        setTitle("Switch Text");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 300);
        setVisible(true);
    }

    public void init() {
        setLayout(null);

        int width = 150;
        int height = 30;

        label1 = new JLabel("Text1: ");
        label1.setBounds(20, 50, width, height);

        label2 = new JLabel("Text2: ");
        label2.setBounds(20, 80, width, height);


        text1 = new JTextField();
        text1.setBounds(100, 50, width, height);
        text1.setText(""); // make sure the text field is empty
        text1.setEditable(false); // as we do not introduce any text at any moment

        text2 = new JTextField();
        text2.setBounds(100, 80, width, height);
        text2.setText(""); // make sure the text field is empty
        text2.setEditable(false); // as we do not introduce any text at any moment

        bSwitch = new JButton("Switch");
        bSwitch.setBounds(90, 150, width, height);

        bSwitch.addActionListener(new Subject2.ImplementSwitchButton());

        add(text1);
        add(text2);
        add(label1);
        add(label2);
        add(bSwitch);
    }

    public static void main(String[] args) {
        new Subject2();
    }

    class ImplementSwitchButton implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (!wasSwitched) {
                text1.setText(INITIAL_TEXT);
                text2.setText("");
                wasSwitched = true;
            } else {
                text2.setText(INITIAL_TEXT);
                text1.setText("");
                wasSwitched = false;
            }
        }
    }
}

